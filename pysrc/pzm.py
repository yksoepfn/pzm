#!/usr/bin/env python

SCREEN_WIDTH = 1500
SCREEN_HEIGHT = 900

CUR_WIDTH = 1500
CUR_HEIGHT = 900

COMMON_SPEED_HORIZ = 1
TICK_TIMEOUT = 50
INVADERS_TIMEOUT = 5000

import enum
import sys
import random
import copy

try:
    from PySide2.QtWidgets import QApplication
    from PySide2.QtWidgets import QMainWindow
    from PySide2.QtWidgets import QLabel
    from PySide2.QtWidgets import QDesktopWidget

    from PySide2.QtGui import QPixmap
    from PySide2.QtGui import QPainter
    from PySide2.QtGui import QFont
    from PySide2.QtGui import QFontMetrics
    from PySide2.QtGui import QColor
    from PySide2.QtGui import QBrush
    from PySide2.QtGui import QPen
    from PySide2.QtGui import QCursor

    from PySide2.QtMultimedia import QSound

    from PySide2.QtCore import QSize
    from PySide2.QtCore import QTimer

    from PySide2.QtCore import Qt

except ImportError:
    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtWidgets import QMainWindow
    from PyQt5.QtWidgets import QLabel
    from PyQt5.QtWidgets import QDesktopWidget

    from PyQt5.QtGui import QPixmap
    from PyQt5.QtGui import QPainter
    from PyQt5.QtGui import QFont
    from PyQt5.QtGui import QFontMetrics
    from PyQt5.QtGui import QColor
    from PyQt5.QtGui import QBrush
    from PyQt5.QtGui import QPen
    from PyQt5.QtGui import QCursor

    from PyQt5.QtMultimedia import QSound

    from PyQt5.QtCore import QSize
    from PyQt5.QtCore import QTimer

    from PyQt5.QtCore import Qt


class Rect():
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def contains(self, x, y):
        if self.x <= x and self.x + self.w >=x:
            if self.y <= y and self.y + self.h >= y:
                return True
        return False

    def overlaps(self, r2):
        if r2.x + r2.w <= self.x or self.x + self.w <= r2.x:
            return False
        elif r2.y + r2.h <= self.y or self.y + self.h <= r2.y:
            return False
        return True


@enum.unique
class Board(enum.IntEnum):
    WELCOME = 0
    MENU = 1
    GAME = 2
    SETTINGS = 3
    SCORES = 4
    HELP = 5
    ABOUT = 6
    NEWSCORE = 7
    QUIT = 8


@enum.unique
class GameState(enum.IntEnum):
    PAUSED = 0
    RUNNING = 1
    FROZEN = 2
    COLLISION = 3
    HIT = 4
    QUIT = 5


@enum.unique
class MenuPos(enum.IntEnum):
    NEWGAME = 0
    SETTINGS = 1
    SCORES = 2
    HELP = 3
    ABOUT = 4
    QUIT = 5


@enum.unique
class Key(enum.IntEnum):
    NONE = 0
    LEFT = 1
    RIGHT = 2
    TOP = 3
    BOTTOM = 4
    ENTER = 5
    ESCAPE = 6
    BACKSPACE = 7
    SPACE = 8
    Q = 9
    E = 10
    B = 11
    O = 12
    R = 13


def drawPixmap(painter, x, y, pixmap):
    x = int(x * CUR_WIDTH / SCREEN_WIDTH)
    y = int(y * CUR_HEIGHT / SCREEN_HEIGHT)
    painter.drawPixmap(
        x, y, pixmap.width(), pixmap.height(), pixmap
    )


class MovableItem(Rect):
    def __init__(self, pixmap, x, launcher=False):
        super().__init__(x, SCREEN_HEIGHT - pixmap.height(), pixmap.width(), pixmap.height())
        self.pixmap = pixmap
        self.marked = False
        self.hasexplosion = False
        self.launcher = launcher

    def width(self):
        return self.w

    def height(self):
        return self.h

    def not_yet(self):
        return self.x > SCREEN_WIDTH

    def already_done(self):
        return self.x + self.w <= 0

    def right_x(self):
        return self.w + self.x

    def is_launcher(self):
        return self.launcher


class MovableList():
    def __init__(self, arena):
        self.movables = []
        self.margin = 15
        self.speed = COMMON_SPEED_HORIZ
        self.arena = arena
        self.generate()

    def append(self, movable):
        self.movables.append(movable)

    def generate(self):
        if len(self.movables) > 0:
            _t = self.movables[-1].right_x()
        else:
            _t = 0
        while _t + self.margin < SCREEN_WIDTH * 1.5:
            m = self.create_random()
            _t = m.right_x()
            self.append(m)

    def create_random(self):
        mov = self.arena.media['images']['movables']
        num = random.randint(0, len(mov)-1)
        m = MovableItem(mov[num],
                        self.movables[-1].right_x() + self.margin if len(self.movables) > 0 else 0)
        return m

    def move(self):
        for elem in self.movables:
            elem.x -= self.speed
        first = self.movables[0]
        if first.already_done():
            self.movables = self.movables[1::]
            self.append(self.create_random())

    def paint(self, painter):
        for elem in self.movables:
            drawPixmap(
                painter,
                elem.x,
                elem.y,
                elem.pixmap
            )

class Ship(Rect):
    def __init__(self, pixmap):
        super().__init__(5, SCREEN_HEIGHT/2 - pixmap.height()/2, pixmap.height(), pixmap.width())
        self.pixmap = pixmap
        #self.x = 5
        #self.y = SCREEN_HEIGHT/2 - pixmap.height()/2

    def paint(self, painter):
        drawPixmap(
            painter,
            self.x,
            self.y,
            self.pixmap)

    def get_spot(self):
        return self.x + 470, self.y + 47

    def get_bspot(self):
        return self.x + 184, self.y + 100


@enum.unique
class MissileDirection(enum.IntEnum):
    L2R = 0
    R2L = 1
    LAUNCHER = 2


class Missile(Rect):
    def __init__(self, x, y, pix, direction):
        super().__init__(x - 10, y - pix.height()/2, pix.width(), pix.height())
        self.pixmap = pix
        self.direction = direction
        self.speedx = 0
        self.speedy = 0
        if direction == MissileDirection.L2R:
            self.speedx = 15
        elif direction == MissileDirection.R2L:
            self.speedx = -15
        elif direction == MissileDirection.LAUNCHER:
            self.speedx = -15
            self.speedy = -15
        self.marked = False

    def move(self):
        self.x += self.speedx
        self.y += self.speedy

    def already_done(self):
        if self.x < 0 or self.x > SCREEN_WIDTH or self.y < 0 or self.y > SCREEN_HEIGHT:
            return True
        return False

    def paint(self, painter):
        drawPixmap(
            painter,
            self.x,
            self.y,
            self.pixmap
        )

    def ship_hit(self, ship):
        pass


class Bomb(Rect):
    def __init__(self, x, y, pix):
        super().__init__(x, y, pix.width(), pix.height())
        #self.x = x
        #self.y = y
        self.pixmap = pix
        #self.w = self.pixmap.width()
        #self.h = self.pixmap.height()
        self.speedx = COMMON_SPEED_HORIZ
        self.speedy = 3
        self.marked = False
        self.mitem = None

    def move(self):
        self.x -= COMMON_SPEED_HORIZ
        self.y += self.speedy

    def paint(self, painter):
        drawPixmap(
            painter,
            self.x,
            self.y,
            self.pixmap)

    def hit(self, mitem: MovableItem):
        if mitem.contains(self.x + self.w/2, self.y + self.h/2):
            self.mitem = mitem
            return True
        return False

    def already_done(self):
        # TODO: obsluzyc zestrzelenie obiektu
        if self.y > SCREEN_HEIGHT or self.x < 0:
            return True
        return False

def rescale_image(img):
    img = QPixmap(img)
    return img.scaled(
        img.width() * CUR_WIDTH / SCREEN_WIDTH,
        img.height() * CUR_HEIGHT / SCREEN_HEIGHT)

def traverse_dict(old, new):
    for e in old:
        if type(old[e]) is dict:
            new[e] = {}
            traverse_dict(old[e], new[e])
        elif type(old[e]) is list:
            new[e] = []
            traverse_list(old[e], new[e])
        elif isinstance(old[e], str):
            new[e] = rescale_image(old[e])

def traverse_list(old, new):
    for e in old:
        if type(e) is dict:
            f = {}
            new.append(f)
            traverse_dict(e, f)
        elif type(e) is list:
            f = []
            new.append(f)
            traverse_list(e, f)
        elif isinstance(e, str):
            new.append(rescale_image(e))

class Arena(QApplication):
    def __init__(self, args):
        super().__init__(args)
        self.media_orig = {
            'images': {
                'background-image': 'images/background.png',
                'background-sky': 'images/blue-sky.jpg',
                'background-checkers': 'images/kratka-1.png',
                'moon-1': 'images/moon-1a.png',
                'movables': ['images/launcher-1a.png',
                             'images/building-1a.png',
                             'images/building-2a.png',
                             'images/building-3a.png',
                             'images/building-4a.png',
                             'images/chimney-1a.png',
                             'images/tree-1a.png',
                             'images/tree-2a.png'],
                'missiles-s': ['images/missile-1a.png',
                             'images/missile-2a.png'],
                'missiles-i': ['images/missilel-1a.png',
                               'images/missilel-2a.png'],
                'missiles-d': ['images/missiled-1a.png',
                               'images/missiled-2a.png'],
                'bombs': ['images/bomb-1a.png'],
                'invaders': ['images/invader-1a.png',
                             'images/invader-2a.png'],
                'ship': 'images/ship-1a.png',
                'menu': [{'plain': 'images/nowa-gra-plain.png',
                          'selected': 'images/nowa-gra-selected.png'},
                         {'plain': 'images/ustawienia-plain.png',
                          'selected': 'images/ustawienia-selected.png'},
                         {'plain': 'images/tabela-rekordow-plain.png',
                           'selected': 'images/tabela-rekordow-selected.png'},
                         {'plain': 'images/pomoc-plain.png',
                          'selected': 'images/pomoc-selected.png'},
                         {'plain': 'images/o-programie-plain.png',
                          'selected': 'images/o-programie-selected.png'},
                         {'plain': 'images/koniec-plain.png',
                          'selected': 'images/koniec-selected.png'}
                ],
                'explosions': ['images/explosion-1a.png',
                            'images/explosion-2a.png',
                            'images/explosion-3a.png',
                            'images/explosion-2a.png',
                            'images/explosion-1a.png'],
                'stars': ['images/star-1.png']
            }
        }
        self.media = {}
        self.timers = {
            'tick': {'timer': QTimer(), 'timeout': TICK_TIMEOUT, 'callback': self.timeout_tick},
            'invaders': {'timer': QTimer(), 'timeout': INVADERS_TIMEOUT, 'callback': self.invaders_tick}
        }

        self.keymapping = {
            Qt.Key_Left: Key.LEFT,
            Qt.Key_Right: Key.RIGHT,
            Qt.Key_Up: Key.TOP,
            Qt.Key_Down: Key.BOTTOM,
            Qt.Key_Enter: Key.ENTER,
            Qt.Key_Return: Key.ENTER,
            Qt.Key_Escape: Key.ESCAPE,
            Qt.Key_Backspace: Key.BACKSPACE,
            81: Key.Q,
            32: Key.SPACE,
            65: Key.LEFT,  # a
            66: Key.B,  # b
            68: Key.RIGHT,  # d
            69: Key.E,  # e
            83: Key.BOTTOM,  # s
            87: Key.TOP,  # w
            79: Key.O,  # o
            82: Key.R # r
        }
        for timer in self.timers:
            t = self.timers[timer]
            t['timer'].timeout.connect(t['callback'])
        sizeObject = QDesktopWidget().screenGeometry(0)
        global CUR_WIDTH
        global CUR_HEIGHT
        CUR_WIDTH = sizeObject.width()
        CUR_HEIGHT = sizeObject.height()
        print(CUR_WIDTH, CUR_HEIGHT)
        traverse_dict(self.media_orig, self.media)

    def timeout_tick(self):
        self.game.timeout_tick()

    def invaders_tick(self):
        self.game.invaders_tick()

    def start_timer(self, timer):
        t = self.timers[timer]
        t['timer'].start(t['timeout'])

    def stop_timer(self, timer):
        t = self.timers[timer]
        t['timer'].stop()
        
    def paint_about(self):
        pass

    def paint_game(self, data):
        self.canvas.paint_game(data)

    def paint_scores(self):
        pass

    def paint_menu(self, data):
        self.canvas.paint_menu(data)

    def paint_newscore(self):
        pass

    def paint_welcome(self):
        self.canvas.paint_welcome()

    def paint_help(self):
        pass


class Invader():
    def __init__(self, x, y, pix):
        self.x = x
        self.y = y
        self.pixmap = pix
        self.w = pix.width()
        self.h = pix.height()
        self.marked = False
        self.hasexplosion = False

    def paint(self, painter):
        drawPixmap(
            painter,
            self.x,
            self.y,
            self.pixmap)

class Star(Rect):
    def __init__(self, x, y, pix):
        super().__init__(x, y, pix.width(), pix.height())
        self.pixmap = pix
        self.speed = -(random.randint(1, 3))
        self.marked = False

    def paint(self, painter):
        drawPixmap(
            painter,
            self.x,
            self.y,
            self.pixmap)

    def move(self):
        self.x += self.speed

    def already_done(self):
        return self.x < 0

class Explosion():
    def __init__(self, x: int , y: int, pixl):
        self.x = x
        self.y = y
        self.pixmaplist = pixl
        self.counter = 0
        self.mitem = None
        self.marked = False

    def already_done(self):
        return self.counter >= len(self.pixmaplist)

    def move(self):
        self.x -= COMMON_SPEED_HORIZ

    def paint(self, painter):
        if self.counter < len(self.pixmaplist):
            pix = self.pixmaplist[self.counter]
            drawPixmap(
                painter,
                self.x - pix.width()/2,
                self.y - pix.height()/2,
                pix)
            self.counter += 1
        else:
            self.marked = True


class Controller(QMainWindow):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.parent.controller = self
        self.showFullScreen()
        self.canvas = Canvas(self, CUR_WIDTH, CUR_HEIGHT, parent)
        self.parent.canvas = self.canvas
        self.setCentralWidget(self.canvas)
        self.setWindowTitle("Spaceshooter vol. 4427")

    def keyPressEvent(self, event):
        key = event.key()
        # print(key)
        text = event.text()
        k = self.parent.keymapping.get(key, None)
        self.game.keypress(k, text)

class Canvas(QLabel):
    def __init__(self, parent, w, h, arena):
        super().__init__(parent)
        self.parent = parent
        self.arena = arena
        self.w = w
        self.h = h
        self.generic_font = QFont("Courier New", 14)
        self.generic_fm = QFontMetrics(self.generic_font)

    def sizeHint(self):
        return QSize(
            self.w,
            self.h)

    def paint_about(self):
        pass

    def paint_game(self, data):
        pi = QPixmap(
            self.w,
            self.h)
        pa = QPainter()
        pa.eraseRect(0, 0, self.w, self.h)
        pa.begin(pi)
        # Start drawing here
        drawPixmap(pa, 0, 0, self.arena.media['images']['background-sky'])
        for s in data['stars']:
            s.paint(pa)
        moon = self.arena.media['images']['moon-1']
        drawPixmap(pa, 
            SCREEN_WIDTH- 200,
            200,
            moon)
        mlist = data['movable-list']
        mlist.paint(pa)
        data['ship'].paint(pa)
        for i in data['invaders']:
            i.paint(pa)
        for m in data['missilesout']:
            m.paint(pa)
        for m in data['missilesin']:
            m.paint(pa)
        for b in data['bombs']:
            b.paint(pa)
        for e in data['explosions_mis']:
            e.paint(pa)
        for e in data['explosions_inv']:
            e.paint(pa)
        # End drawing here
        pa.end()
        self.setPixmap(pi)

    def paint_menu(self, data):
        pi = QPixmap(
            self.w,
            self.h)
        pa = QPainter()
        pa.begin(pi)
        # Start drawing here
        drawPixmap(pa, 0, 0, self.arena.media['images']['background-checkers'])
        _t = 0
        pa.setFont(self.generic_font)
        _pos = data['menu-position']
        for pos in self.arena.media['images']['menu']:
            if _pos == _t:
                p = pos['selected']
            else:
                p = pos['plain']
            drawPixmap(pa, 300, 100 + 120 * _t, p)
            #     pa.setBrush(QBrush(QColor("#dfe322"), Qt.SolidPattern))
            # else:
            #     pa.setBrush(QBrush(QColor("#777777"), Qt.SolidPattern))
            # pa.drawRect(100, 100 + 70*_t, SCREEN_WIDTH-200, 50)
            # pa.drawText(110, 110 + 70*_t, pos['title'])
            _t += 1
        # End drawing here
        pa.end()
        self.setPixmap(pi)

    def paint_newscore(self):
        pass

    def paint_welcome(self):
        pi = QPixmap(
            self.w,
            self.h)
        pa = QPainter()
        pa.begin(pi)
        # Start drawing here
        drawPixmap(
            pa, 0, 0,
            self.arena.media['images']['background-checkers'])
        pa.setFont(self.generic_font)
        pa.drawText(100, 100, "Witajcie z Matplanety")
        # End drawing here
        pa.end()
        self.setPixmap(pi)

    def paint_help(self):
        pass


class Game():
    def __init__(self, arena, canvas):
        self.arena = arena
        canvas.game = self
        self.board = Board.QUIT
        self.state = GameState.PAUSED
        self.menupos = 0
        self.missilesout = []
        self.missilesin = []
        self.stars = []
        self.invaders = []
        self.explosions_mis = []
        self.explosions_inv = []
        self.bombs = []
        self.boards = {
            Board.WELCOME: self.trigger_welcome,
            Board.MENU: self.trigger_menu,
            Board.GAME: self.trigger_game,
            Board.SETTINGS: self.trigger_settings,
            Board.SCORES: self.trigger_scores,
            Board.HELP: self.trigger_help,
            Board.ABOUT: self.trigger_about,
            Board.NEWSCORE: self.trigger_newscore,
            Board.QUIT: self.trigger_quit}

        self.menu= [{"title": "NOWA GRA", "board": Board.GAME},
                    {"title": "USTAWIENIA", "board": Board.SETTINGS},
                    {"title": "TABELA REKORDOW", "board": Board.SCORES},
                    {"title": "POMOC", "board": Board.HELP},
                    {"title": "O PROGRAMIE", "board": Board.ABOUT},
                    {"title": "KONIEC", "board": Board.QUIT}]

        self.counters = {
            'tick': 0
        }
        self.states = {
            GameState.PAUSED: self.game_paused,
            GameState.RUNNING: self.game_running,
            GameState.FROZEN: self.game_frozen,
            GameState.COLLISION: self.game_collision,
            GameState.HIT: self.game_hit,
            GameState.QUIT: self.game_quit}

        self.paintfuncs = {
            Board.WELCOME: self.paint_welcome,
            Board.MENU: self.paint_menu,
            Board.GAME: self.paint_game,
            Board.HELP: self.paint_help,
            Board.SCORES: self.paint_scores,
            Board.ABOUT: self.paint_about,
            Board.NEWSCORE: self.paint_newscore}

        self.keypressfuncs = {
            Board.WELCOME: self.keypress_welcome,
            Board.GAME: self.keypress_game,
            Board.ABOUT: self.keypress_about,
            Board.MENU: self.keypress_menu,
            Board.SCORES: self.keypress_scores,
            Board.HELP: self.keypress_help,
            Board.NEWSCORE: self.keypress_newscore
        }

        self.mlist = MovableList(self.arena)
        self.change_board(Board.MENU)
        self.ship = Ship(self.arena.media['images']['ship'])
        self.create_stars()
        self.create_invaders()

    def create_explosion(self, mitem: MovableItem):
        if not mitem.hasexplosion:
            e = Explosion(mitem.x + mitem.w/2, mitem.y + mitem.h/2, self.arena.media['images']['explosions'])
            self.explosions_mis.append(e)
            e.mitem = mitem
            mitem.hasexplosion = True

    def create_invaders(self):
        for i in range(4):
            j = Invader(SCREEN_WIDTH - 300, 50 + i * 150, random.choice(self.arena.media['images']['invaders']))
            self.invaders.append(j)

    def create_star(self):
        y = random.randint(1, 12) * (SCREEN_HEIGHT - 300)/12
        offset = random.randint(0, (SCREEN_HEIGHT - 300)/12)
        y += offset
        s = Star(SCREEN_WIDTH, y, self.arena.media['images']['stars'][0])
        self.stars.append(s)

    def create_stars(self):
        self.stars = []
        for _ in range(12):
            x = random.randint(0, SCREEN_WIDTH)
            y = random.randint(1, 12) * (SCREEN_HEIGHT - 300)/12
            offset = random.randint(0, (SCREEN_HEIGHT - 300)/12)
            y += offset
            s = Star(x, y, self.arena.media['images']['stars'][0])
            self.stars.append(s)

    def create_ship_missile(self, index):
        x, y = self.ship.get_spot()
        m = Missile(x, y, self.arena.media['images']['missiles-s'][index], MissileDirection.L2R)
        self.missilesout.append(m)

    def create_missilein(self, x, y, direction):
        m = None
        if direction == MissileDirection.R2L:
            m = Missile(x, y, self.arena.media['images']['missiles-i'][0], direction)
        elif direction == MissileDirection.LAUNCHER:
            m = Missile(x, y, self.arena.media['images']['missiles-d'][0], direction)
        self.missilesin.append(m)

    def create_bomb(self):
        x,y = self.ship.get_bspot()
        b = Bomb(x, y, self.arena.media['images']['bombs'][0])
        self.bombs.append(b)

    def keypress(self, key, text):
        self.keypressfuncs[self.board](key, text)

    def keypress_welcome(self, key, text):
        if key == Key.Q:
            sys.exit(0)
        else:
            self.change_board(Board.MENU)

    def keypress_menu(self, key, text):
        if key == Key.Q:
            sys.exit(0)
        elif key == Key.TOP:
            if self.menupos > 0:
                self.menupos-= 1
                self.paint_menu()
        elif key == Key.BOTTOM:
            if self.menupos < len(self.menu) - 1:
                self.menupos += 1
                self.paint_menu()
        elif key == Key.ENTER:
            self.change_board(self.menu[self.menupos]['board'])

    def keypress_game(self, key, text):
        if key == Key.Q or key == Key.ESCAPE:
            self.stop_game()
            self.change_board(Board.MENU)
        elif key == Key.BOTTOM:
            if self.ship.y < SCREEN_HEIGHT - 10:
                self.ship.y += 10
        elif key == Key.TOP:
            if self.ship.y > 10:
                self.ship.y -= 10
        elif key == Key.LEFT:
            if self.ship.x > 10:
                self.ship.x -= 10
        elif key == Key.RIGHT:
            if self.ship.x < SCREEN_WIDTH / 4:
                self.ship.x += 10
        elif key == Key.SPACE:
            self.create_ship_missile(1)
        elif key == Key.R:
            self.create_ship_missile(0)
        elif key == Key.B:
            self.create_bomb()

    def keypress_scores(self, key, text):
        pass

    def keypress_help(self, key, text):
        pass

    def keypress_about(self, key, text):
        pass

    def keypress_newscore(self, key, text):
        pass

    def trigger_settings(self):
        pass

    def trigger_welcome(self):
        self.paint_welcome()

    def trigger_menu(self):
        self.paint_menu()

    def trigger_game(self):
        self.arena.start_timer('tick')
        self.arena.start_timer('invaders')
        self.paint_game()

    def stop_game(self):
        self.arena.stop_timer('invaders')
        self.arena.stop_timer('tick')

    def trigger_scores(self):
        self.paint_scores()
        pass

    def trigger_help(self):
        self.paint_help()

    def trigger_about(self):
        self.paint_about()

    def trigger_newscore(self):
        self.paint_newscore()

    def trigger_quit(self):
        sys.exit(0)

    def game_paused(self):
        pass

    def game_running(self):
        pass

    def game_frozen(self):
        pass

    def game_collision(self):
        pass

    def game_hit(self):
        pass

    def game_quit(self):
        pass

    def invaders_tick(self):
        if len(self.missilesin) < 4 and len(self.invaders) > 0:
            invader = random.choice(self.invaders)
            self.create_missilein(
                invader.x,
                invader.y + invader.h/2,
                MissileDirection.R2L
            )

    def timeout_tick(self):
        self.counters['tick'] += 1
        self.counters['tick'] %= 2
        self.mlist.move()
        self.paint_game()
        for m in self.missilesout:
            m.move()
            for i in self.invaders:
                if not i.hasexplosion:
                    if m.overlaps(i):
                        self.create_explosion(i)
                        i.marked = True
                        m.marked = True
            if m.already_done():
                m.marked = True
        for m in self.missilesin:
            m.move()
            if m.already_done():
                m.marked = True
        for s in self.stars:
            s.move()
            if s.already_done():
                s.marked = True
                self.create_star()
        for b in self.bombs:
            b.move()
            if b.already_done():
                b.marked = True
        for b in self.bombs:
            if not b.mitem:
                for m in self.mlist.movables:
                    if not m.hasexplosion:
                        if b.hit(m):
                            self.create_explosion(m)
                            b.marked = True
        for e in self.explosions_mis:
            if e.already_done():
                e.marked = True
                e.mitem.marked = True
        for e in self.explosions_inv:
            if e.already_done():
                e.marked = True
                e.mitem.marked = True

        self.bombs = [x for x in self.bombs if not x.marked]
        self.missilesout = [x for x in self.missilesout if not x.marked]
        self.missilesin = [x for x in self.missilesin if not x.marked]
        self.stars = [x for x in self.stars if not x.marked]
        self.mlist.movables = [x for x in self.mlist.movables if not x.marked]
        self.explosions_mis = [x for x in self.explosions_mis if not x.marked]
        self.explosions_inv = [x for x in self.explosions_inv if not x.marked]
        self.invaders = [x for x in self.invaders if not x.marked]

    def paint_welcome(self):
        self.arena.paint_welcome()

    def paint_menu(self):
        data = {}
        data['menu-position'] = self.menupos
        data['menu'] = self.menu
        self.arena.paint_menu(data)

    def paint_game(self):
        data = {'tick': self.counters['tick'],
                'movable-list': self.mlist,
                'ship': self.ship,
                'missilesout': self.missilesout,
                'missilesin': self.missilesin,
                'stars': self.stars,
                'invaders': self.invaders,
                'bombs': self.bombs,
                'explosions_mis': self.explosions_mis,
                'explosions_inv': self.explosions_inv
                }
        self.arena.paint_game(data)

    def paint_scores(self):
        self.arena.paint_scores()

    def paint_help(self):
        self.arena.paint_help()

    def paint_about(self):
        self.arena.paint_about()

    def paint_newscore(self):
        self.arena.paint_newscore()
    
    def change_board(self, board):
        if self.board != board:
            self.board = board
            self.boards[board]()

    def paint(self):
        self.paintfuncs[self.board]()


if __name__ == "__main__":
    random.seed()
    arena = Arena(sys.argv)
    window = Controller(arena)
    game = Game(arena, window.canvas)
    arena.game = game
    window.game = game
    window.canvas.game = game
    sys.exit(arena.exec_())
